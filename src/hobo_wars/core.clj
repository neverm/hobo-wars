(ns hobo-wars.core
  (:require [ring.adapter.jetty :as jetty]
            [ring.middleware.reload :refer [wrap-reload]]
            [ring.util.response :refer [redirect]]
            [compojure.core :as compojure :refer [wrap-routes]]
            [compojure.route :refer [not-found]]
            [hobo-wars.layout :refer [home-page error-page error-response]]
            [hobo-wars.middleware :as mw]
            [hobo-wars.game.combat :as combat]
            [hobo-wars.game.player :refer [svyatoslavy absurd]]))

(def game (atom {}))
(def combat (combat/make-combat [svyatoslavy] [absurd]))

(defn reset-game! [] (reset! game {:combat combat}))
(reset-game!)

(defn make-turn! []
  (let [turn (combat/make-turn svyatoslavy absurd)
        next-combat (combat/next-turn (combat/submit-turn turn (:combat @game)))]
    (swap! game assoc :combat next-combat)))

(defn get-combat-log []
  (get-in @game [:combat :log]))

(compojure/defroutes home-routes
  (compojure/GET "/" request (home-page (get-combat-log)))
  (compojure/POST "/hit" request (do (make-turn!)
                                     (redirect "/"))))

(def app
  (mw/wrap-base
   (compojure/routes
    (wrap-routes #'home-routes mw/wrap-csrf)
    home-routes
    (not-found (error-page "404: not found" "The requested page was not found")))))

(defn -main []
  (jetty/run-jetty
   (-> #'app
       wrap-reload)
   {:port 3001
    :join? false}))
