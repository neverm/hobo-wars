(ns hobo-wars.game.combat)

;; todo: move damage calculations to a separate file
(defn- randrange
  "Generate random integer in range from `min` to `max` inclusive"
  [min max]
  (+ min (rand-int (- (+ max 1) min))))

(defn- generate-hit-message
  "Generate hit message using names of `attacker` and `defender` and the damage done"
  [attacker defender damage]
  (str (:name attacker) " hits " (:name defender) " for " damage))

(defn hit [attacker defender]
  "Generate single attack hit by `attacker` on `defender`."
  (let [damage (randrange 5 10)]
    {:damage damage
     :message (generate-hit-message attacker defender damage)}))

(defn make-combat [side-a side-b]
  {:side-a side-a :side-b side-b :log [] :submitted-turns []})

(defn submit-turn
  "Submit given turn to the combat. Return updated combat"
  [turn combat]
  (update combat :submitted-turns conj turn))

(defn make-turn
  "Make a single turn of attacker against defender"
  [attacker defender]
  {:attacker attacker :defender defender})

(defn perform-turn
  "Perform given `turn`.
  Return `turn` updated with its outcome: hit or miss of the attacker
  against the defender"
  [{:keys [attacker defender] :as turn}]
  (assoc turn :hit (hit attacker defender)))


(defn next-turn
  "Calculate next turn from submitted turns.
  Calculate outcomes of every submitted turn, update state of the combatants
  such as hp removal from damage. Remove combatants that are dead. Update combat log
  with all the events that happened during this turn.
  Return updated combat"
  [{:keys [side-a side-b log submitted-turns] :as combat}]
  (let [new-log-entries
        (map (comp :message :hit) (map perform-turn submitted-turns))]
    (-> combat
        (assoc :submitted-turns [])
        (assoc :log (into log new-log-entries)))))
