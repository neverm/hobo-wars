(ns hobo-wars.game.player)

(def svyatoslavy
  {:name "svyatoslavy"
   :level 1
   :hp 100
   :total-hp 100
   :weapon "Fists"})

(def absurd
  {:name "absurd"
   :level 80
   :hp 18000
   :total-hp 18000
   :weapon "Stones"})
