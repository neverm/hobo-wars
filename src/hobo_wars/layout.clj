(ns hobo-wars.layout
  (:require [selmer.parser :as parser]
            [ring.middleware.anti-forgery :refer [*anti-forgery-token*]]
            [ring.util.anti-forgery :refer [anti-forgery-field]]))

(parser/set-resource-path! (clojure.java.io/resource "html"))
(parser/add-tag! :csrf-field (fn [_ _] (anti-forgery-field)))

(defn- render-template
  "Render given html template to string.
  Use map of params to substitute variables inside template body"
  [template-name params]
  (parser/render-file
   template-name
   (assoc params
          :csrf-token *anti-forgery-token*)))

(defn home-page
  "Render home page layout.
  `hits` is a sequence of hits performed"
  [hits]
  (render-template "home.html" {:hits (reverse hits)}))

(defn error-page
  "Render error page layout.
  `title` title of the page
  `message` message to be displayed"
  [title message]
  (render-template "error.html" {:error-name title :error-text message}))

(defn error-response
  "Create error response based on given error-details
  `error-details` should be a map containing the following keys:
  :status - error status
  :title - error title (optional)
  :message - detailed error message (optional)

  Return a response map with the error page rendered as the body and
  the status specified by the status key"
  [error-details]
  (let [default-text (str "Error: " (:status error-details))
        title (or (:title error-details) default-text)
        message (or (:message error-details) default-text)]
    {:status (:status error-details)
     :headers {"Content-Type" "text/html; charset=utf-8"}
     :body (error-page title message)}))
