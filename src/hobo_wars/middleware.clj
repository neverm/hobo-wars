(ns hobo-wars.middleware
  (:require [ring.middleware.defaults :refer [site-defaults wrap-defaults]]
            [ring.middleware.anti-forgery :refer [wrap-anti-forgery]]
            [hobo-wars.layout :refer [error-response]]))

(defn wrap-csrf [handler]
  (wrap-anti-forgery
   handler
   {:error-response
    (error-response
     {:status 403
      :title "Invalid anti-forgery token"})}))

(defn wrap-base
  "Base middleware that unifies other middlewares common for every request."
  [handler]
  (-> handler
      (wrap-defaults
       (assoc-in site-defaults [:security :anti-forgery] false))))
